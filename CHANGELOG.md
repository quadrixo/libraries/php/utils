# Version 2.4.0 - 2022-04-13
## Changed
- `ContainerHelper::instantiate()` doesn't check container before creating new instance
## Removed
- `ContainerHelper::instanciate()`

# Version 2.3.3 - 2022-04-02
## Added
- Add `ContainerHelper::isInstantiable()`
## Changed
- `ContainerHelper::instanciate()` is marked as obsolete and replaced by `ContainerHelper::instantiate()`

# Version 2.3.2 - 2022-03-25
## Added
- Add `ContainerHelper::getArguments()`

# Version 2.3.1 - 2021-09-27
## Added
- Add `str_underscorize`

# Version 2.3.0 - 2021-09-11
## Added
- Add `ContainerHelper` which provide usefull method using container

# Version 2.0.0 - 2021-04-14
## Added
- Add `str_pascalize` is upper camel case
- `str_camelize` accept world separator characters
## Changed
- `str_camelize` is now lower camel case
- `str_camelize` and `str_dasherize` aren't backward compatible

# Version 1.1.0 - 2021-04-14
## Added
- Add `str_camelize`
- Add `str_dasherize`

# Version 1.0.0 - 2021-04-01
## Added
- Add `str_contains` for PHP < 8
- Add `str_ends_with` for PHP < 8
- Add `str_starts_with` for PHP < 8
