[![pipeline status](https://gitlab.com/quadrixo/libraries/php/utils/badges/main/pipeline.svg)](https://gitlab.com/quadrixo/libraries/php/utils/-/commits/main) [![coverage report](https://gitlab.com/quadrixo/libraries/php/utils/badges/main/coverage.svg)](https://gitlab.com/quadrixo/libraries/php/utils/-/commits/main)

# quadrixo/utils

`quadrixo/utils` is library which contains usefull things.

## PHP 8 new functions

Add to PHP < 8 some usefull function new in PHP 8.

 - `get_debug_type(mixed $value): string`: Gets the type name of a variable in a way that is suitable for debugging.
 - `str_contains(string $haystack, string $needle): bool`: Determine if a string contains a given substring.
 - `str_ends_with(string $haystack, string $needle): bool`: Checks if a string ends with a given substring.
 - `str_starts_with(string $haystack, string $needle): bool`: Checks if a string starts with a given substring.

## String functions

### str_camelize

```php
str_camelize(string $text, string $separators = "_- \t\r\n\f\v"): string
```

`str_camelize` transforms a text into its lower camel case representation.

The definition of a word is any string of characters that is immediately after any character listed in the separators parameter (By default these are: blanks, dash '-' and underscore '_').

### str_dasherize

```php
str_dasherize(string $text): string
```

`str_dasherize` transforms a text by separating worlds with a dash.

The definition of a word is any string of characters that starts with an upper case letter.

### str_pascalize

```php
str_pascalize(string $text, string $separators = "_- \t\r\n\f\v"): string
```

`str_pascalize` transforms a text into its upper camel case representation.

The definition of a word is any string of characters that is immediately after any character listed in the separators parameter (By default these are: blanks, dash '-' and underscore '_').

## License

This software is licensed under the [CeCILL](https://cecill.info/) license.

[CeCILL-2.1](./LICENSE)
