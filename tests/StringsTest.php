<?php
declare(strict_types=1);
namespace StringsTest;

use PHPUnit\Framework\TestCase;

class StringsTest extends TestCase
{
    /**
     * @dataProvider provideTextForCamelize
     */
    public function testCamelize(string $separators, string $text, string $expected)
    {
        $actual = str_camelize($text, $separators);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideTextForDasherize
     */
    public function testDasherize(string $text, string $expected)
    {
        $actual = str_dasherize($text);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideTextForPascalize
     */
    public function testPascalize(string $separators, string $text, string $expected)
    {
        $actual = str_pascalize($text, $separators);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideTextForUnderscorize
     */
    public function testUnderscorize(string $text, string $expected)
    {
        $actual = str_underscorize($text);
        $this->assertEquals($expected, $actual);
    }

    public function provideTextForCamelize()
    {
        return [
            [ "_- \t\r\n\f\v", 'e', 'e' ],
            [ "_- \t\r\n\f\v", '_E', 'e' ],
            //[ "_- \t\r\n\f\v", '-é', 'é' ],
            [ "_- \t\r\n\f\v", 'background-color', 'backgroundColor' ],
            [ "_- \t\r\n\f\v", ' background color', 'backgroundColor' ],
            [ "_- \t\r\n\f\v", 'BackgroundColor', 'backgroundColor' ]
        ];
    }

    public function provideTextForDasherize()
    {
        return [
            [ 'E', '-e' ],
            [ '_e', '_e' ],
            //[ '-É', '-É' ],
            [ 'BackgroundColor', '-background-color' ],
            [ ' BackgroundColor', ' -background-color' ],
            [ 'background-color', 'background-color' ],
            [ '_background-color', '_background-color' ]
        ];
    }

    public function provideTextForPascalize()
    {
        return [
            [ "_- \t\r\n\f\v", 'e', 'E' ],
            [ "_- \t\r\n\f\v", '_E', 'E' ],
            [ "_- \t\r\n\f\v", '-é', 'é' ],
            [ "_- \t\r\n\f\v", 'background-color', 'BackgroundColor' ],
            [ "_- \t\r\n\f\v", ' background color', 'BackgroundColor' ],
            [ "_- \t\r\n\f\v", 'BackgroundColor', 'BackgroundColor' ]
        ];
    }

    public function provideTextForUnderscorize()
    {
        return [
            [ 'E', '_e' ],
            [ '-e', '-e' ],
            [ 'BackgroundColor', '_background_color' ],
            [ ' BackgroundColor', ' _background_color' ],
            [ 'background_color', 'background_color' ],
            [ '-background_color', '-background_color' ]
        ];
    }
}
