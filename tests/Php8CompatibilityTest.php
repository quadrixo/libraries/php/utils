<?php
declare(strict_types=1);
namespace Php8CompatibilityTest;

use PHPUnit\Framework\TestCase;

class Php8CompatibilityTest extends TestCase
{
    private const HAYSTACK = 'This the haystack for test with some &é"\'(-è_çà';

    public function testStrContains()
    {
        $this->assertTrue(str_contains(static::HAYSTACK, ''));
        $this->assertTrue(str_contains(static::HAYSTACK, 'some'));
        $this->assertTrue(str_contains(static::HAYSTACK, "'"));
        $this->assertFalse(str_contains(static::HAYSTACK, "Hello"));
    }

    public function testStrEndsWith()
    {
        $this->assertTrue(str_ends_with(static::HAYSTACK, ''));
        $this->assertTrue(str_ends_with(static::HAYSTACK, 'some &é"\'(-è_çà'));
        $this->assertFalse(str_ends_with(static::HAYSTACK, 'This'));
    }

    public function testStrEndsWithNull()
    {
        $this->assertTrue(str_ends_with(static::HAYSTACK, null));
        $this->assertTrue(str_ends_with(null, null));
        $this->assertTrue(str_ends_with('', null));
        $this->assertTrue(str_ends_with(null, ''));
    }

    public function testSrStartsWith()
    {
        $this->assertTrue(str_starts_with(static::HAYSTACK, ''));
        $this->assertTrue(str_starts_with(static::HAYSTACK, ''));
        $this->assertTrue(str_starts_with(static::HAYSTACK, 'This'));
        $this->assertTrue(str_starts_with(static::HAYSTACK, 'This the haystack for test with some &é"\'(-è_çà'));
        $this->assertFalse(str_starts_with(static::HAYSTACK, 'è_çà'));
    }

    public function testSrStartsWithNull()
    {
        $this->assertTrue(str_starts_with(static::HAYSTACK, null));
        $this->assertTrue(str_starts_with(null, null));
        $this->assertTrue(str_starts_with('', null));
        $this->assertTrue(str_starts_with(null, ''));
    }
}
