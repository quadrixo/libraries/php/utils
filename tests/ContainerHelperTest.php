<?php
declare(strict_types=1);
namespace ContainerHelperTest;

use ContainerHelper;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use ReflectionException;

class ContainerHelperTest extends TestCase
{
    /** @var ContainerInterface */
    private $container;

    protected function setUp(): void
    {
        $this->container = new class() implements ContainerInterface
        {
            public function get(string $id)
            {
                if (class_exists($id))
                {
                    return new $id();
                }
                throw new InvalidArgumentException();
            }

            public function has(string $id): bool
            {
                return class_exists($id);
            }
        };
    }

    public function testSimpleInstance()
    {
        $foo = ContainerHelper::instantiate($this->container, Foo::class);
        $this->assertInstanceOf(Foo::class, $foo);
    }

    public function testComplexInstance()
    {
        $bar = ContainerHelper::instantiate($this->container, Bar::class);
        $this->assertInstanceOf(Bar::class, $bar);
        $this->assertInstanceOf(Foo::class, $bar->foo);
    }

    public function testNewParameterInstance()
    {
        $bar = ContainerHelper::instantiate($this->container, Bar::class);
        $baz = ContainerHelper::instantiate($this->container, Bar::class);

        $this->assertNotSame($bar->foo, $baz->foo);
    }

    public function testParameterOverride()
    {
        $foo = new Foo();
        $bar = ContainerHelper::instantiate($this->container, Bar::class, [ 'foo' => $foo ]);
        $this->assertSame($foo, $bar->foo);
    }

    public function testReflectionException()
    {
        $this->expectException(ReflectionException::class);
        $this->expectExceptionMessage("Class None does not exist");
        ContainerHelper::instantiate($this->container, 'None');
    }

    public function testAbstractClass()
    {
        $this->expectError();
        $this->expectErrorMessage("Cannot instantiate abstract class " . AbstractFoo::class);
        ContainerHelper::instantiate($this->container, AbstractFoo::class);
    }
}

abstract class AbstractFoo
{
}

class Foo
{
}

class Bar
{
    public $foo;

    public function __construct(Foo $foo)
    {
        $this->foo = $foo;
    }
}
