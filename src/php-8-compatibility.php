<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/web-application library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2021 Luc Chante - All rights reserved
 */

if (!function_exists('get_debug_type'))
{
    function get_debug_type($value): string
    {
        if (is_resource($value))
        {
            $type = get_resource_type($value);
            if ($type === 'Unknown')
            {
                $type = 'closed';
            }
            return "resource ($type)";
        }
        if ($value instanceof __PHP_Incomplete_Class)
        {
            return '__PHP_Incomplete_Class';
        }
        if (!is_object($value))
        {
            return gettype($value);
        }

        $class = get_class($value);
        if (!str_contains($class, '@'))
        {
            return $class;
        }

        $name = get_parent_class($class) ?: key(class_implements($class)) ?: 'class';
        return "$name@anonymous";
    }
}

if (!function_exists('str_contains'))
{
    function str_contains(?string $haystack, ?string $needle): bool
    {
        return empty($needle) || strpos($haystack, $needle) !== false;
    }
}

if (!function_exists('str_ends_with'))
{
    function str_ends_with(?string $haystack, ?string $needle): bool
    {
        return empty($needle) || substr_compare($haystack, $needle, -strlen($needle)) == 0;
    }
}

if (!function_exists('str_starts_with'))
{
    function str_starts_with(?string $haystack, ?string $needle): bool
    {
        return empty($needle) || strncmp($haystack, $needle, strlen($needle)) == 0;
    }
}
