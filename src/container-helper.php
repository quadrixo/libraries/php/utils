<?php
declare(strict_types = 1);

use Psr\Container\ContainerInterface;

final class ContainerHelper
{
    private function __construct() { }

    /**
     * Returns the injected value of the given parameters
     * @param ContainerInterface $container
     * @param ReflectionParameter[] $parameters
     * @param array<string, mixed> $overrideParams
     * @return array
     */
    public static function getArguments(ContainerInterface $container, array $parameters, array $overrideParams = [])
    {
        assert(array_reduce($parameters, fn($c, $p) => $c && is_a($p, ReflectionParameter::class), true));

        return array_map(function(ReflectionParameter $p) use($container, $overrideParams)
        {
            $name = $p->getName();
            if (array_key_exists($name, $overrideParams))
            {
                return $overrideParams[$name];
            }

            $type = $p->getType();
            if ($type instanceof ReflectionNamedType)
            {
                $id = $type->getName();
                if ($container->has($id))
                {
                    return $container->get($id);
                }
            }

            if ($p->isDefaultValueAvailable())
            {
                return $p->getDefaultValue();
            }
            else if ($p->allowsNull())
            {
                return null;
            }

            throw new InvalidArgumentException("The parameter '{$p->getName()}' cannot be resolved");
        },
        $parameters);
    }

    /**
     * Execute the closure and returns its result.
     *
     * The parameters of the closure are resolve using the given container.
     *
     * @param ContainerInterface $container
     * @param Closure $closure
     * @return mixed
     */
    public static function execute(ContainerInterface $container, Closure $closure, array $overrideParams = [])
    {
        $function = new ReflectionFunction($closure);
        $parameters = self::getArguments($container, $function->getParameters(), $overrideParams);
        return $closure(...$parameters);
    }

    /**
     * Returns if the given classname is instantiable
     * @param string $classname
     * @return bool
     */
    public static function isInstantiable(string $classname)
    {
        if (!class_exists($classname))
        {
            return false;
        }
        $class = new ReflectionClass($classname);
        return $class->isInstantiable();
    }

    /**
     * Create a new instance of the given class.
     *
     * The parameters of the constructor are resolved using the given container.
     * `$overrideParams` can be used to override the parameters resolsution.
     *
     * @param ContainerInterface $container
     * @param string $classname
     * @param array $overrideParams
     * @return mixed
     */
    public static function instantiate(ContainerInterface $container, string $classname, array $overrideParams = [])
    {
        $class = new ReflectionClass($classname);
        $constructor = $class->getConstructor();
        if (!$constructor)
        {
            return new $classname();
        }

        $instance = $class->newInstanceWithoutConstructor();
        ContainerHelper::execute($container, $constructor->getClosure($instance), $overrideParams);
        return $instance;
    }
}
