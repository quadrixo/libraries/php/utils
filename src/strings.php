<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/web-application library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2021 Luc Chante - All rights reserved
 */

if (!function_exists('str_camelize'))
{
    /**
     * Convert a text to its camel case replesentation.
     *
     * @param string $text The text to transform.
     * @param string $separators Word separator characters.
     * @return string
     */
    function str_camelize(string $text, string $separators = "_- \t\r\n\f\v"): string
    {
        return lcfirst(str_replace(str_split($separators), '', ucwords($text, $separators)));
    }
}

if (!function_exists('str_dasherize'))
{
    /**
     * Convert text into lowercase with words separed with a dash.
     *
     * @param string $text
     * @return string
     */
    function str_dasherize(string $text): string
    {
        return strtolower(preg_replace('/([[:upper:]])/', '-$1', $text));
    }
}

if (!function_exists('str_pascalize'))
{
    /**
     * Convert a text to its pascal case replesentation.
     *
     * @param string $text The text to transform.
     * @param string $separators Word separator characters.
     * @return string
     */
    function str_pascalize(string $text, string $separators = "_- \t\r\n\f\v"): string
    {
        return str_replace(str_split($separators), '', ucwords($text, $separators));
    }
}

if (!function_exists('str_underscorize'))
{
    /**
     * Convert text into lowercase with words separed with an underscore.
     *
     * @param string $text
     * @return string
     */
    function str_underscorize(string $text): string
    {
        return strtolower(preg_replace('/([[:upper:]])/', '_$1', $text));
    }
}
