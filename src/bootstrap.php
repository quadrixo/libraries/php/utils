<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/web-application library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2021 Luc Chante - All rights reserved
 */

require_once __DIR__ . DIRECTORY_SEPARATOR . 'php-8-compatibility.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'strings.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'container-helper.php';
